<?php

namespace CarerixConnection;

use Carerix\Api\Rest\Client;
use Carerix\Api\Rest\Manager;
use Carerix\Api\Rest\Entity;
  
// Timezone setting
date_default_timezone_set('Europe/Amsterdam');

// initialize client
$client = new Client();

// initialize client manager
$manager = new Manager($client);

// auto-discover all built-in entities
$manager->autoDiscoverEntities();

// set system name
$manager->setUsername('MY_SYSTEM_NAME');

// set user token
$manager->setPassword('TOKEN_RECEIVED_FROM_CARERIX_SUPPORT');

// set proxy (optional, can be used for HTTP traffic debugging or if your service is behind a firewall)
// if you don't know what it is just ignore it.
//$manager->setProxy('localhost:8888');

// register entity manager
Entity::setManager($manager);