<?php

namespace CarerixConnection\Admin;

use CarerixConnection\Api\Fetch;

class Synchronize {

    /**
     * Fetch active publications, insert new records and update existing ones
     */
    public function sync_vacancies() {

        $fetch = new Fetch();
        $publications = $fetch->fetch_publications();

        if (class_exists('ACF')) {
            foreach ($publications as $key => $publication) {
                
                // Check state of publication and remove from array if no allowed state could be determined
                $state = $this->get_state($publication);

                // If no state is found, remove this publication from the array
                if(!$state) {

                    unset($publications[$key]);
                    
                } 
                
                // If a state is found, continue vacancy creation
                else {

                    // Create query to find existing vacancy by publicationID
                    $find_vacancy_query = $this->find_vacancy_query($publication['publicationID']);

                    // If no posts are found, create a new vacancy
                    if($find_vacancy_query->found_posts == 0) {

                        $vacancy_id = $this->create_vacancy($publication);

                    } else {
                        // If a post is found, get it and update it
                        $vacancy_id = $find_vacancy_query->posts[0];
                        $this->update_vacancy($vacancy_id, $publication);

                    }
                    
                    // Find state term and connect to the vacancy
                    $state_term = get_term_by('slug', $state, 'state');
                    if($state_term) {
                        wp_set_object_terms($vacancy_id, $state_term->slug, 'state');
                        update_field('state_meta', $state_term->term_id, $vacancy_id);
                    }
                    
                    // Create or find location taxonomy and connect to the vacancy
                    if($location_id = $this->find_or_create_location($publication)) {
                        wp_set_object_terms($vacancy_id, intval($location_id), 'location');
                    }

                    // Find employment taxonomy and connect to the vacancy
                    if($employment_id = $this->find_or_create_employment($publication)) {
                        wp_set_object_terms($vacancy_id, intval($employment_id), 'employment');
                    }

                    // Find branche taxonomy and connect to the vacancy
                    if($branche_id = $this->find_or_create_branche($publication)) {
                        wp_set_object_terms($vacancy_id, intval($branche_id), 'branche');
                    }

                    // Find discipline taxonomy and connect to the vacancy
                    if($discipline_id = $this->find_or_create_discipline($publication)) {
                        wp_set_object_terms($vacancy_id, intval($discipline_id), 'discipline');
                    }      
                }      
                
            }

            // Get all vacancies and delete the ones that aren't in the latest fetch
            $all_vacancy_ids = get_posts(array(
                'numberposts' => -1,
                'post_type' => 'vacancy',
                'fields' => 'ids'
            ));

            foreach($all_vacancy_ids as $vacancy_id) {
                $publication_id = get_post_meta($vacancy_id, 'publication_id', true);

                // If the publication_id exists and is not found in the publications array, delete the vacancy
                if(!empty($publication_id) && array_search($publication_id, array_column($publications, 'publicationID')) === false) {
                    wp_delete_post($vacancy_id, true);
                }
            }

            // Store current time as vacancies_updated_at time
            $updated_at = current_time('mysql');
            update_option('vacancies_updated_at', $updated_at);
        }

        // Return publications data as json
        wp_send_json([
            'updated_at' => wp_date('j F Y H:i:s', strtotime($updated_at)),
            'data' => $publications
        ]);
    }

    /**
     * Create query to find vacancy ids by publication_id
     * @param  string|int $publication_id 
     * @return WP_Query                 
     */
    private function find_vacancy_query($publication_id) {

        $find_vacancy_query = new \WP_Query(array(
            'numberposts'   => 1,
            'post_type'     => 'vacancy',
            'meta_key'      => 'publication_id',
            'meta_value'    => intval($publication_id),
            'fields'        => 'ids'
        ));

        return $find_vacancy_query;
    }

    /**
     * Insert a new vacancy with publication data
     * @param  array $publication 
     * @return int   vacancy post id
     */
    private function create_vacancy($publication) {

        $locationString = array_key_exists('workLocation', $publication['toVacancy']) ? '-' . $publication['toVacancy']['workLocation'].'-' : false;

        $new_vacancy_id = wp_insert_post(array(
            'post_type' => 'vacancy',
            'post_title' => $publication['titleInformation'],
            'post_name' => $publication['toVacancy']['jobTitle'].$locationString.'-'.$publication['toVacancy']['vacancyID'],
            'post_status' => 'publish',
            'post_author' => 1
        ));

        // Run update method to fill custom fields
        $this->update_vacancy_fields($new_vacancy_id, $publication);

        return $new_vacancy_id;

    }

    /**
     * Update existing vacancy with publication data
     * @param  int $vacancy_id  
     * @param  array $publication 
     */
    private function update_vacancy($vacancy_id, $publication) {

        $vacancy_id = wp_update_post(array(
            'ID' => $vacancy_id,
            'post_title' => $publication['titleInformation'],
            'post_date' => $publication['creationDate']
        ));

        // Run update method to fill custom fields
        $this->update_vacancy_fields($vacancy_id, $publication);

    }

    /**
     * Update custom fields for vacancy with publication data
     * @param  int $vacancy_id  
     * @param  array $publication 
     */
    private function update_vacancy_fields($vacancy_id, $publication) {

        $vacancy = $publication['toVacancy'];

        // Update meta fields
        
        // Publication id
        update_field('publication_id', $publication['publicationID'], $vacancy_id);
        
        // Vacancy number
        update_field('vacancy_no', $vacancy['vacancyNo'], $vacancy_id);
        
        // Valid through date
        if(array_key_exists('deadlineMaterial', $publication)) {
            update_field('valid_through', $publication['deadlineMaterial'], $vacancy_id);
        }

        // Publication start
        update_field('publicationStart', $publication['publicationStart'], $vacancy_id);

        // StreetAddress
        if(array_key_exists('workStreet', $vacancy)) {
            $streetAddress = $vacancy['workStreet'];
            $streetAddress .= isset($vacancy['workNumber']) ? ' ' . $vacancy['workNumber'] : '';
            update_field('streetAddress', $streetAddress, $vacancy_id);
        }

        // Province
        if(isset($vacancy['toProvince1Node']['value'])) {
            update_field('province', $vacancy['toProvince1Node']['value'], $vacancy_id);    
        }

        // Postal code
        if(array_key_exists('workPostalCode', $vacancy)) {
            update_field('postal_code', $vacancy['workPostalCode'], $vacancy_id);    
        }

        // Country
        if(array_key_exists('value', $vacancy['toCountry1Node'])) {
            update_field('country', $vacancy['toCountry1Node']['value'], $vacancy_id);    
        }

        // Function
        if(array_key_exists('value', $vacancy['toFunctionLevel2'])) {
            update_field('function', $vacancy['toFunctionLevel2']['value'], $vacancy_id);    
        }

        // Work level
        if(array_key_exists('value', $vacancy['toWorkLevelNode'])) {
            update_field('level', $vacancy['toWorkLevelNode']['value'], $vacancy_id);    
        }
        
        // Hours
        update_field('hours', $vacancy['hoursPerWeek'], $vacancy_id);

        // Check if logo key exists and create the img src string
        $logo_base64 = '';
        if(array_key_exists('logo', $vacancy['toCompany'])) {
            $logo_base64 = $this->get_base64_src_string($this->get_value_or_empty($vacancy['toCompany']['logo'], 'content'));
        }
        // Salary
        update_field('salary', [
            'period' => $this->get_value_or_empty($vacancy['toSalaryPeriodNode'], 'value'),
            'currency' => $this->get_value_or_empty($vacancy['toSalaryScaleCurrencyNode'], 'value'),
            'min' => $this->get_value_or_empty($vacancy, 'minSalary'),
            'max' => $this->get_value_or_empty($vacancy, 'maxSalary')
        ], $vacancy_id);

        // Company
        update_field('company', [
            'anonymous' => $this->get_value_or_empty($vacancy, 'isAnonymous'),
            'id' => $this->get_value_or_empty($vacancy['toCompany'], 'companyID'),
            'name' => $this->get_value_or_empty($vacancy['toCompany'], 'name'),
            'logo_base64' => $logo_base64
        ], $vacancy_id);

        // Update main content fields
        update_field('hero', [
            'title' => $vacancy['jobTitle'],
            'intro' => $this->get_value_or_empty($publication, 'introInformation')
        ], $vacancy_id);

        update_field('content', [
            'companyInformation' => $this->get_value_or_empty($publication, 'companyInformation'),
            'vacancyInformation' => $this->get_value_or_empty($publication, 'vacancyInformation'),
            'requirementsInformation' => $this->get_value_or_empty($publication, 'requirementsInformation'),
            'offerInformation' => $this->get_value_or_empty($publication, 'offerInformation'),
            'contactInformation' => $this->get_value_or_empty($publication, 'functionContactInformation')

        ], $vacancy_id);
        
        // Find owner and link to vacancy
        if($team_member = $this->find_team_member($publication, 'owner')) {
            update_field('owner', $team_member, $vacancy_id);
        }

        // Find brought_in_by and link to vacancy
        if($team_member = $this->find_team_member($publication, 'broughtInBy')) {
            update_field('brought_in_by', $team_member, $vacancy_id);
        }
    }

    /**
     * Find or create a employment taxonomy by productNode value from publication. Return false if no productnode exists
     * @param  array $publication
     * @return int|boolean
     */
    private function find_or_create_employment($publication) {

        // Get the productNode if it exists in the array
        $product_node = array_key_exists('toProductNode', $publication['toVacancy']) ? $publication['toVacancy']['toProductNode'] : false;

        if(!$product_node) {
            return false;
        }

        $employment = get_term_by('slug', sanitize_title($product_node['value']), 'employment');

        // If employment taxonomy isn't found, create a new taxonomy and update the slug field with the dataNodeID
        if(!$employment) {

            $employment = wp_insert_term($product_node['value'], 'employment');

            return $employment['term_id'];
        }

        return $employment->term_id;
    }

    /**
     * Find or create a branche taxonomy by brancheLevel1 value from vacancy. Return false if no brancheLevel1 exists
     * @param  array $publication
     * @return int|boolean
     */
    private function find_or_create_branche($publication) {

        // Get the brancheLevel1 if it exists in the array
        $branche_level = array_key_exists('toBrancheLevel1', $publication['toVacancy']) ? $publication['toVacancy']['toBrancheLevel1'] : false;

        if(!$branche_level || !array_key_exists('value', $branche_level)) {
            return false;
        }

        $branche = get_term_by('slug', sanitize_title($branche_level['value']), 'branche');

        // If branche taxonomy isn't found, create a new taxonomy and update the slug field with the dataNodeID
        if(!$branche) {

            $branche = wp_insert_term($branche_level['value'], 'branche');

            return $branche['term_id'];
        }

        return $branche->term_id;
    }

    /**
     * Find or create a location taxonomy by workLocation value from publication. Return false if no workLocation exists
     * @param  array $publication
     * @return int|boolean
     */
    private function find_or_create_location($publication) {

        // Get the location string if it exists in the array
        $locationString = array_key_exists('workLocation', $publication['toVacancy']) ? $publication['toVacancy']['workLocation'] : false;

        if(!$locationString) {
            return false;
        }

        // Sanitize and generalize locationString
        $locationString = rtrim(explode('/', $locationString)[0]);

        $location = get_term_by('name', $locationString, 'location');

        // If location taxonomy isn't found, create a new taxonomy
        if(!$location) {

            $location = wp_insert_term($locationString, 'location');

            return $location['term_id'];
        }

        return $location->term_id;
    }

    /**
     * Find or create a discipline taxonomy by functionLevel1 value from publication. Return false if no functionLevel1 exists
     * @param  array $publication
     * @return int|boolean
     */
    private function find_or_create_discipline($publication) {

        // Get the FunctionLevel1 if it exists in the array
        $function_level = array_key_exists('toFunctionLevel1', $publication['toVacancy']) ? $publication['toVacancy']['toFunctionLevel1'] : false;

        if(!$function_level || $function_level['dataNodeID'] == '0') {
            return false;
        }

        $discipline = get_term_by('slug', sanitize_title($function_level['value']), 'discipline');

        // If discipline taxonomy isn't found, create a new taxonomy and update the slug field with the dataNodeID
        if(!$discipline) {

            $discipline = wp_insert_term($function_level['value'], 'discipline');

            return $discipline['term_id'];
        }

        return $discipline->term_id;
    }


    /**
     * Find the local team_member where carerix_user_id matches the owner userID of the publication
     * @param  array $publication 
     * @param  string $field
     * @return WP_Post team_member post_type
     */
    private function find_team_member($publication, $field) {

        $carerix_user_id = array_key_exists($field, $publication['toVacancy']) ? $publication['toVacancy'][$field]['userID'] : false;

        if(!$carerix_user_id) {
            return false;
        }

        // Get the teammember by carerix_user_id meta_value
        $team_member = get_posts(array(
            'numberposts'   => 1,
            'post_type'     => 'team_member',
            'meta_key'      => 'carerix_user_id',
            'meta_value'    => intval($carerix_user_id),
            'fields'        => 'ids'
        ));

        // Return the first element or false
        return reset($team_member);
    }
    

    /**
     * Extract mimetype from base64 string and prepend it to the string for an img src attribute
     * @param  string $string 
     * @return string         
     */
    private function get_base64_src_string($string) {

        if(empty($string)) {
            return '';
        }
        
        $imgdata = base64_decode($string);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);

        return "data:{$mime_type};base64, {$string}";

    }

    /**
     * Return value if it exists in an array, otherwise return empty string
     *
     * @param array $array
     * @param string $key
     * @return string
     */
    private function get_value_or_empty($array, $key) {
        return array_key_exists($key, $array) ? $array[$key] : '';
    }


    /**
     * Return state value of publication
     *
     * @param array $publication
     * @return boolean|string false, filled, active, closed or expected
     */
    private function get_state($publication) {

        /** Status IDs
         * 1: Offerte uitgebracht
         * 2: Actief exclusief
         * 3: Ingevuld
         * 5649: Actief niet-exclusief
         * */

         $state = false;
         $dayStart = date('Y-m-d') . ' 00:00:00';

        if(array_key_exists('toStatusNode', $publication['toVacancy'])) {

            $statusID = $publication['toVacancy']['toStatusNode']['dataNodeID'];
            $hasDeadlineMaterial = array_key_exists('deadlineMaterial', $publication);

            //  State is 'filled'
            if($statusID == 3) {

                $state = 'ingevuld';

            }

            if($statusID == 2 || $statusID == 5649) {
                
                // State is 'active'
                if(!$hasDeadlineMaterial) {

                    $state = 'actueel';

                } else {
                    
                    // State is 'active'
                    if($publication['deadlineMaterial'] >= $dayStart) {

                        $state = 'actueel';

                    } else {

                        // State is 'closed'
                        $state = 'reactietermijn-gesloten';

                    }
                }
            }

            // State is 'expected'
            if($statusID == 1 && (!$hasDeadlineMaterial || $publication['deadlineMaterial'] >= $dayStart)) {

                $state = 'verwacht';

            }

        }

        return $state;
    }
}