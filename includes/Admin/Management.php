<?php

namespace CarerixConnection\Admin;

use CarerixConnection\Admin\Synchronize;

class Management {

    var $menu_id;

    public function __construct() {

        // Add management page and enqueue scripts
        add_action('admin_menu', array($this, 'add_management_page'));
        add_action('admin_enqueue_scripts', array($this, 'admin_enqueues'));

        // Callback for sync vacancies ajax call
        add_action('wp_ajax_sync_vacancies', array($this, 'sync_vacancies'));

    }

    /**
     * Add management page for Carerix
     */
    public function add_management_page() {
        $this->menu_id = add_management_page(
            __('Carerix Beheer', 'carerix_connection'),
            __('Carerix Beheer', 'carerix_connection'),
            'manage_options',
            'carerix-management',
            array( $this, 'management_page' )
        );
    }

    /**
     * Include management page template
     */
    public function management_page() {

        $updated_at = wp_date('j F Y H:i:s', strtotime(get_option('vacancies_updated_at')));
        include_once(plugin_dir_path( __FILE__ ) . 'templates/management.php');

    }

    /**
     * Enqueue management page scripts
     * @param  string $hook_suffix 
     *
     */
    public function admin_enqueues($hook_suffix) {

        if ($hook_suffix != $this->menu_id) {
            return;
        }

        wp_enqueue_script('management', plugins_url('assets/js/management.js', __FILE__), array('jquery'));
    }

    /**
     * Callback for manual sync button. Instantiate Synchronize class and run sync_vacancies
     */
    public function sync_vacancies() {
        
        $synchronize = new Synchronize();

        $synchronize->sync_vacancies();
    }
}