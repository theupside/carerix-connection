<div class='wrap'>
    <h1>Carerix Instellingen</h1>
    <form action='options.php' method='post'>
        <?php
        settings_fields( 'carerix-connection' );
        do_settings_sections( 'carerix-settings' );
        submit_button();
        ?>
    </form>
</div>