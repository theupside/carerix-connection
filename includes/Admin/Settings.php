<?php

namespace CarerixConnection\Admin;

class Settings {

    const SETTINGS_NAME = 'carerix-connection-settings';
    private $options;

    public function __construct() {

        // $this->settingsName = 'carerix-connection-settings';
        $this->options = get_option( self::SETTINGS_NAME );

        // Register admin options page
        add_action( 'admin_menu', array( $this, 'add_options_page' ) );
        add_action( 'admin_init', array( $this, 'options_init' ) );

    }

    public static function get_username() {

        return get_option(self::SETTINGS_NAME)['username'] ?? '';

    }

    public static function get_token() {

        return get_option(self::SETTINGS_NAME)['token'] ?? '';
        
    }

    public static function get_medium() {

        return get_option(self::SETTINGS_NAME)['medium'] ?? 'web';

    }

    public static function get_team_domains() {

        return get_option(self::SETTINGS_NAME)['team_domains'] ?? '';

    }

    /**
     * Register options page for Carerix Connection settings
     * @return void 
     */
    public function add_options_page() {

        add_options_page(
            __('Carerix Instellingen', 'carerix_connection'),
            __('Carerix Instellingen', 'carerix_connection'),
            'manage_options', 
            'carerix-settings',
            array( $this, 'options_page' )
        );

    }


    public function options_init() {
        // Register settings
        register_setting( 'carerix-connection', self::SETTINGS_NAME);

        // Add section
        add_settings_section(
            'carerix-connection-settings-section',
            __( 'Connectie met Carerix', 'carerix_connection' ),
            array( $this, 'add_settings_section' ),
            'carerix-settings'
        );

        // Add fields to section
        add_settings_field(
            'carerix_connection_username',
            __( 'Gebruikersnaam', 'carerix_connection' ),
            array( $this, 'carerix_connection_username' ),
            'carerix-settings',
            'carerix-connection-settings-section'
        );

        add_settings_field(
            'carerix_connection_token',
            __( 'Application token', 'carerix_connection' ),
            array( $this, 'carerix_connection_token' ),
            'carerix-settings',
            'carerix-connection-settings-section'
        );

        add_settings_field(
            'carerix_connection_medium',
            __( 'Medium', 'carerix_connection' ),
            array( $this, 'carerix_connection_medium' ),
            'carerix-settings',
            'carerix-connection-settings-section'
        );

        add_settings_field(
            'carerix_connection_team_domains',
            __( 'Domeinen van teamlid email-adressen', 'carerix_connection' ),
            array( $this, 'carerix_connection_team_domains' ),
            'carerix-settings',
            'carerix-connection-settings-section'
        );
    }

    public function add_settings_section() {}

    public function options_page() {

        include_once(plugin_dir_path( __FILE__ ) . 'templates/settings-form.php');

    }

    public function carerix_connection_username() {

        $value = $this->options['username'] ?? '';
        printf('<input name="%s[username]" type="text" class="large-text" value="%s" />', self::SETTINGS_NAME, $value);

    }

    public function carerix_connection_token() {

        $value = $this->options['token'] ?? '';
        printf('<input name="%s[token]" type="text" class="large-text" value="%s" />', self::SETTINGS_NAME, $value);

    }

    public function carerix_connection_medium() {

        $value = $this->options['medium'] ?? '';
        printf('<input name="%s[medium]" type="text" class="large-text" value="%s" />', self::SETTINGS_NAME, $value);

    }

    public function carerix_connection_team_domains() {

        $value = $this->options['team_domains'] ?? '';
        printf('<input name="%s[team_domains]" type="text" class="large-text" value="%s" />', self::SETTINGS_NAME, $value);
        print('<p class="description">Voer een door komma\'s gescheiden lijst van domeinen in waar teamlid email-adressen gebruikt in Carerix op kunnen eindigen. (het deel na de @)</p>');

    }
}
