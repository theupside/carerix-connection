(function ($) {

    $(function(){

        $('#sync-carerix-publications').click(handleSynchButtonClick);

    });
    

    function handleSynchButtonClick() {
        $syncDateElement = $('#last-sync .date');
        $spinnerElement = $('#last-sync .spinner');

        $syncDateElement.html('');
        $spinnerElement.addClass('is-active');

        $.get({

            url : ajaxurl,
            data : {

              'action': 'sync_vacancies',

            },

        })

        .done(function(response) {

            $syncDateElement.html(response.updated_at);
            
            console.log(response);

        })

        .always(function(response) {

            $spinnerElement.removeClass('is-active');

        });
    }

})(jQuery);

