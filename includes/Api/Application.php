<?php

namespace CarerixConnection\Api;

use Carerix\Api\Rest\Entity\CREmployee;
use Carerix\Api\Rest\Entity\CRMatch;
use Carerix\Api\Rest\Entity\CRAttachment;
use Carerix\Api\Rest\Entity\CRPublication;
use Carerix\Api\Rest\Entity;

class Application {

    private $values;
    private $employee;

    public function __construct($values) {

        $this->values = $values;
        $this->setup_carerix_connection();

    }

    /**
     * Instantiate Carerix connection
     */
    public function setup_carerix_connection() {

        new Connect();

    }

    /**
     * Find or create an employee, attach the uploaded CV file, execute the application to create a match, attach motivation to match
     * @return void
     */
    public function apply() {

        // Find or create employee
        $this->employee = $this->find_or_create_employee();

        // Attach CV
        $this->attach_cv_to_employee();

        // Apply for the job
        // x-cx-dedupe: 0 = create duplicate CREmployee if the emailaddress exists, 1 = overwrite existing CREmployee
        // // 'x-cx-dedupe' => ($this->values['known_member'] == 'on') ? 1 : 0
        $this->employee = $this->employee->apply([
            'x-cx-pub' => $this->values['publication_id']
        ]);

        // Attach motivation to match
        $this->attach_motivation_to_match();

        // Set state to match
        $this->set_state_to_match();
        

    }

    /**
     * Find an existing CREmployee by emailaddress or create a new CREmployee using the values
     * @return CREmployee
     */
    private function find_or_create_employee() {

        // Find employee by email address if the known_member checkbox is on
        if($this->values['known_member'] == 'on') {

            // Try to find an employee by email
            $employees = CREmployee::findAll([
                'qualifier' => sprintf("toUser.emailAddresses.emailAddress like '%s'", $this->values['email']),
                'show' => [
                    'id',
                    'emailAddress'
                ]
            ], Entity::HYDRATE_OBJECT);

            // Return the first record if employees are found
            $employee = ($employees && $employees->getTotalCount()) ? $employees[0] : null;

        }

        // Create an employee if it doesn't exist yet
        if(!isset($employee) || !$employee) {

            $employee = new CREmployee();
            $employee
                ->setEmailAddress($this->values['email'])
                ->setFirstName($this->values['first_name'])
                ->setLastName($this->values['last_name'])
                ->setLastNamePrefix($this->values['last_name_prefix'])
                ->setMobileNumber($this->values['phone']);          

        }

        // Update consent
        // Give consent
        $employee->setToConsentStageNode(array('attributes' => array('key' => 'tags', 'value' => 'ConsentGivenTag')));

        // Set approval date to today
        $employee->setPrivacyApprovalDate(date('Y-m-d h:i:s'));

        // Set period to 1 year if it is not set 
        $currentConsentPeriodNode = $employee->getToConsentPeriodNode();
        
        if(!$currentConsentPeriodNode || $currentConsentPeriodNode->sortOrder < 40){ // 1 year CRDataNode has a sortOrder of 40
            $employee->setToConsentPeriodNode(array('attributes' => array('key' => 'tags', 'value' => 'OneYearTag')));
        }

        return $employee;
    }

    /**
     * If a cv file is present, create an CRAttachment and add it to the employee setAttachments
     * @return void
     */
    private function attach_cv_to_employee() {

        // Create a CV attacment and attach to employee
        if(array_key_exists('cv', $this->values)) {

            $cv_data = array(
               'filePath' => $this->values['cv']['name'],
               'content' => base64_encode(file_get_contents($this->values['cv']['tmp_name'])),
               'label'  => 'cv',
               'toTypeNode' => array('id' => 60)
            );

            $cv_attachment = new CRAttachment($cv_data);
            $this->employee->setAttachments([$cv_attachment]);
        }

    }

    /**
     * Find the match and attach motivation text and motivation file as attachment to it
     * @return void
     */
    private function attach_motivation_to_match() {

        // Find the match
        $match = $this->find_match();

        if($match) {
            
            // Set motivation text            
            $match->setMotivation($this->values['motivation_text']);

            // Attach motivation file
            if(array_key_exists('motivation_file', $this->values)) {

                $motivation_data = array(
                   'filePath' => $this->values['motivation_file']['name'],
                   'content' => base64_encode(file_get_contents($this->values['motivation_file']['tmp_name'])),
                   'label'  => 'Motivatie',
                   'toTypeNode' => array('id' => 6860)
                );

                $motivation_attachment = new CRAttachment($motivation_data);
                $match->setAttachments([$motivation_attachment]);
                
            }

            $match->save();
        }

    }

    /**
     * Set statusInfo with displayName as state to match
     * @return void
     */
    private function set_state_to_match() {
        if($state = $this->values['state']){
            // Find the match
            $match = $this->find_match();

            if($match) {
                $match->setStatusInfo(['displayName' => $state]);

                $match->save();

            }
        }
    }

    /**
     * Find a CRMatch by combining the employee id and vacancy id.
     * @return CRMatch 
     */
    private function find_match() {

        // Get the employeeID from the class employee instance
        $employee_id = $this->employee->employeeID;
        // Find the publication and get the vacancy id
        $publication = CRPublication::find($this->values['publication_id'], [
            'show' => ['toVacancy.id']
        ], Entity::HYDRATE_OBJECT_ARRAY);
        $vacancy_id = $publication['toVacancy']['vacancyID'];

        // Create the match id by concatenating vacancy and employee id with a period
        $match_id = $vacancy_id . '.' . $employee_id; 

        // Find and return the CRMatch instance
        return CRMatch::find($match_id);

    }
}