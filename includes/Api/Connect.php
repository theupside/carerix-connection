<?php

namespace CarerixConnection\Api;

use Carerix\Api\Rest\Client;
use Carerix\Api\Rest\Manager;
use Carerix\Api\Rest\Entity;
use CarerixConnection\Admin\Settings;

class Connect {

    public function __construct() {

        $this->init();
    }

    public function init() {

        // Timezone setting
        date_default_timezone_set('Europe/Amsterdam');

        // initialize client
        $client = new Client();

        // initialize client manager
        $manager = new Manager($client);

        // auto-discover all built-in entities
        $manager->autoDiscoverEntities();

        // set system name
        $manager->setUsername(Settings::get_username());

        // set user token
        $manager->setPassword(Settings::get_token());

        // set proxy (optional, can be used for HTTP traffic debugging or if your service is behind a firewall)
        // if you don't know what it is just ignore it.
        //$manager->setProxy('localhost:8888');

        // register entity manager
        Entity::setManager($manager);

        do_action('connection_setup');

    }
}