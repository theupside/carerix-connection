<?php

namespace CarerixConnection\Api;

use Carerix\Api\Rest\Entity\CRPublication;
use Carerix\Api\Rest\Entity\CRUser;
use Carerix\Api\Rest\Entity\CRCompany;
use Carerix\Api\Rest\Entity;
use CarerixConnection\Admin\Settings;

class Fetch {

    public function __construct() {

        $this->setup_carerix_connection();

    }

    /**
     * Instantiate Carerix connection
     */
    public function setup_carerix_connection() {

        new Connect();

    }

    /**
     * Find all active publications in Carerix
     * @return array 
     */
    public function fetch_publications() {
        
        $medium = Settings::get_medium();
        $now = date('Y-m-d H:i:s');
        // $todayNoon = date('Y-m-d') . ' 12:00:00';
        $dayStart = date('Y-m-d') . ' 00:00:00';

        $qualifier = "toMedium.code = '{$medium}'
                      AND publicationStart <= (NSCalendarDate) '{$now}'
                      AND (publicationEnd > (NSCalendarDate) '{$dayStart}' OR publicationEnd = nil)
                      AND toVacancy.toStatusNode.isHidden = 0
                      AND toStatusNode.dataNodeID != 5343";

        $publications = CRPublication::findAll([
            'qualifier' => $qualifier,
            'count' => 999999,
            'ordering' => '({key=creationDate;sel=Ascending})',
            'show' => [
                'creationDate',
                'deadlineMaterial',
                'publicationStart',
                'toVacancy.toStatusNode.dataNodeID',
                'toVacancy.toProvince1Node.value',
                'toVacancy.workPostalCode',
                'toVacancy.toCountry1Node.value',
                'toVacancy.jobTitle',
                'toVacancy.vacancyNo',
                'toVacancy.toCompany.name',
                'toVacancy.toCompany.logo',
                'toVacancy.toCompany.companyID',
                'toVacancy.isAnonymous',
                'toVacancy.hoursPerWeek',
                'toVacancy.toProductNode',
                'toVacancy.workLocation',
                'toVacancy.workStreet',
                'toVacancy.workNumber',
                'toVacancy.toWorkLevelNode',
                'toVacancy.toFunctionLevel1',
                'toVacancy.toFunctionLevel2',
                'toVacancy.toBrancheLevel1',
                'titleInformation',
                'introInformation',
                'companyInformation',
                'vacancyInformation',
                'requirementsInformation',
                'offerInformation',
                'functionContactInformation',
                'toVacancy.owner.userID',
                'toVacancy.broughtInBy.userID',
                'toVacancy.toSalaryPeriodNode.value',
                'toVacancy.toSalaryScaleCurrencyNode.value',
                'toVacancy.minSalary',
                'toVacancy.maxSalary',
            ]
        ], Entity::HYDRATE_OBJECT_ARRAY);

        return $publications;
        
    }

    /**
     * Fetch and return firstName and lastName of all CRUsers that are active and have a onlyhuman.nl email address
     * @return array 
     */
    public function fetch_team_ids() {

        $domains = str_replace(' ', '', Settings::get_team_domains());
        $emailAddressBusiness = '';

        foreach(explode(',', $domains) as $key => $domain) {
            $connectionWord = $key == 0 ? 'AND' : 'OR';
            $emailAddressBusiness .= sprintf(" %s emailAddressBusiness like '*@%s'", $connectionWord, $domain);
        }

        $qualifier = "isActive = 1".$emailAddressBusiness;
        $team_ids = CRUser::findAll([
            'qualifier' => $qualifier,
            'count' => 999999,
            'ordering' => '({key=lastName;sel=Ascending})',
            'show' => [
                'firstName',
                'lastName',
                'lastNamePrefix'
            ]
        ], Entity::HYDRATE_ARRAY);

        return $team_ids['CRUser'];
    }
}