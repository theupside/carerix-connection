<?php

namespace CarerixConnection;

use StoutLogic\AcfBuilder\FieldsBuilder;


// Content fields
$content = new FieldsBuilder('vacancy', [
    'position' => 'acf_after_title',
    'title' => __('Pagina-inhoud', 'carerixconnection'),
    'hide_on_screen' => [
        'the_content',
        'discussion',
        'comments',
        'revisions',
        'slug',
        'author',
        'format',
        'categories',
        'tags',
        'send',
        'featured_image'
    ]
]);

$content
    ->setLocation('post_type', '==', 'vacancy');

$content
    ->addTab('Hero', ['placement' => 'left'])
    ->addGroup('hero', ['label' => '', 'wpml_cf_preferences' => 0])
        ->addText('title', ['label' => 'Titel', 'wpml_cf_preferences' => 2])
        ->addWysiwyg('intro', [
            'label' => 'Introductie',
            'toolbar' => 'essential',
            'media_upload' => 0,
            'wpml_cf_preferences' => 2
        ])
    ->endGroup();

$content
    ->addTab('Inhoud', ['placement' => 'left'])
    ->addGroup('content', ['label' => '', 'wpml_cf_preferences' => 0])
        ->addWysiwyg('companyInformation', [
            'label' => 'Organisatie',
            'toolbar' => 'paragraph',
            'media_upload' => 0,
            'wpml_cf_preferences' => 2
        ])
        ->addWysiwyg('vacancyInformation', [
            'label' => 'Functie',
            'toolbar' => 'paragraph',
            'media_upload' => 0,
            'wpml_cf_preferences' => 2
        ])
        ->addWysiwyg('requirementsInformation', [
            'label' => 'Functie-eisen',
            'toolbar' => 'paragraph',
            'media_upload' => 0,
            'wpml_cf_preferences' => 2
        ])
        ->addWysiwyg('offerInformation', [
            'label' => 'Aanbod',
            'toolbar' => 'paragraph',
            'media_upload' => 0,
            'wpml_cf_preferences' => 2
        ])
        ->addWysiwyg('contactInformation', [
            'label' => 'Contact',
            'toolbar' => 'paragraph',
            'media_upload' => 0,
            'wpml_cf_preferences' => 2
        ])
    ->endGroup();

// Meta fields
$meta = new FieldsBuilder('vacancy-meta', [
    'position' => 'side',
    'title' => __('Eigenschappen', 'carerixconnection'),
]);

$meta
    ->setLocation('post_type', '==', 'vacancy');

$meta
    ->addField('publication_id', 'acfe_hidden', ['wpml_cf_preferences' => 2])
    ->addField('vacancy_no', 'acfe_hidden', ['wpml_cf_preferences' => 2]) // toVacancy.vacancyNo

    ->addField('valid_through', 'acfe_hidden', ['wpml_cf_preferences' => 2]) //deadlineMaterial
    ->addField('publicationStart', 'acfe_hidden', ['wpml_cf_preferences' => 2]) //publicationStart
    ->addField('streetAddress', 'acfe_hidden', ['wpml_cf_preferences' => 2]) //toVacancy.workStreet + toVacancy.workNumber
    ->addField('province', 'acfe_hidden', ['wpml_cf_preferences' => 2]) //toVacancy.toProvince1Node.value
    ->addField('postal_code', 'acfe_hidden', ['wpml_cf_preferences' => 2]) //toVacancy.workPostalCode
    ->addField('country', 'acfe_hidden', ['wpml_cf_preferences' => 2]) //toVacancy.toCountry1Node.value
    ->addField('state_meta', 'acfe_hidden', ['wpml_cf_preferences' => 2])
    
    ->addTaxonomy('state', ['label' => 'Status', 'taxonomy' => 'state', 'field_type' => 'select', 'required' => 1, 'add_term' => 0, 'save_terms' => 1, 'load_terms' => 1, 'wpml_cf_preferences' => 2])
    ->addTaxonomy('employment', ['label' => 'Dienstverband', 'taxonomy' => 'employment', 'field_type' => 'select', 'required' => 1, 'add_term' => 0, 'save_terms' => 1, 'load_terms' => 1, 'wpml_cf_preferences' => 2])
    ->addTaxonomy('branche', ['label' => 'Sector', 'taxonomy' => 'branche', 'field_type' => 'select', 'required' => 1, 'add_term' => 0, 'save_terms' => 1, 'load_terms' => 1, 'wpml_cf_preferences' => 2])
    ->addTaxonomy('discipline', ['label' => 'Vakgebied', 'taxonomy' => 'discipline', 'field_type' => 'select', 'required' => 1, 'add_term' => 0, 'save_terms' => 1, 'load_terms' => 1, 'wpml_cf_preferences' => 2])
    ->addTaxonomy('location', ['label' => 'Regio', 'taxonomy' => 'location', 'field_type' => 'select', 'required' => 1, 'add_term' => 0, 'save_terms' => 1, 'load_terms' => 1, 'wpml_cf_preferences' => 2])

    ->addText('function', ['label' => 'Functie', 'wpml_cf_preferences' => 2])
    ->addText('level', ['label' => 'Niveau', 'wpml_cf_preferences' => 2])
    ->addText('hours', ['label' => 'Uren per week', 'wpml_cf_preferences' => 2])
    ->addGroup('salary', ['label' => 'Salaris', 'wpml_cf_preferences' => 0])
        ->addText('period', ['label' => 'Periode', 'wpml_cf_preferences' => 2])
        ->addText('currency', ['label' => 'Valuta', 'wpml_cf_preferences' => 2])
        ->addText('min', ['label' => 'Minimaal', 'wpml_cf_preferences' => 2])
        ->addText('max', ['label' => 'Maximaal', 'wpml_cf_preferences' => 2])
    ->endGroup()

    ->addPostObject('owner', ['label' => 'Contactpersoon', 'post_type' => 'team_member', 'wpml_cf_preferences' => 2])
    ->addPostObject('brought_in_by', ['label' => 'Ingebracht door', 'post_type' => 'team_member', 'wpml_cf_preferences' => 2]);

$meta
    ->addGroup('company', ['label' => 'Bedrijf', 'wpml_cf_preferences' => 0])
        ->addTrueFalse('anonymous', ['label' => 'Anoniem', 'wpml_cf_preferences' => 2])
        ->addText('id', ['label' => 'Carerix ID', 'wpml_cf_preferences' => 2])
        ->addText('name', ['label' => 'Naam', 'wpml_cf_preferences' => 2])
        ->addField('logo_base64', 'acfe_hidden', ['wpml_cf_preferences' => 2])
        ->addImage('logo', ['label' => 'Logo', 'wpml_cf_preferences' => 2])
            ->setInstructions('Het logo uit Carerix wordt op een andere manier getoond, hier kun je handmatig een logo uploaden voor vacatures die niet uit Carerix komen.')
    ->endGroup();
    

/**
 * Add ACF Builder fields
 */
if( function_exists('acf_add_local_field_group') ) {

    add_action('acf/init', function () use ($content, $meta) {

        acf_add_local_field_group($content->build());
        acf_add_local_field_group($meta->build());

    });
}