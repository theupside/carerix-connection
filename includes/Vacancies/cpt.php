<?php

namespace CarerixConnection;

add_action( 'init', __NAMESPACE__ . '\register_vacancy', 0 );

// Set custom admin columns
add_filter( 'manage_vacancy_posts_columns', __NAMESPACE__ . '\set_vacancy_admin_columns' );
add_action( 'manage_vacancy_posts_custom_column' , __NAMESPACE__ . '\fill_vacancy_admin_columns', 10, 2 );

function register_vacancy() {
    $vacancy_labels = array(
        'name'                  => __( 'Vacatures' ),
        'singular_name'         => __( 'Vacature' ),
        'add_new'               => __( 'Nieuwe vacature toevoegen' ),
        'add_new_item'          => __( 'Nieuwe vacature toevoegen' ),
        'edit_item'             => __( 'Bewerk vacature' ),
        'new_item'              => __( 'Nieuwe vacature' ),
        'view_item'             => __( 'Vacature bekijken' ),
        'view_items'            => __( 'Bekijk vacatures' ),
        'search_items'          => __( 'Zoek vacatures' ),
        'all_items'             => __( 'Alle vacatures' ),
        'archives'              => __( 'Vacature-archief' ),
        'attributes'            => __( 'Vacature-attributen' ),
        'insert_into_item'      => __( 'Invoegen bij vacature' ),
        'uploaded_to_this_item' => __( 'Geupload naar vacature' ),
        'filter_items_list'     => __( 'Vacature-lijst filter' ),
        'items_list_navigation' => __( 'Vacature-lijst navigatie' ),
        'items_list'            => __( 'Vacature-lijst' ),
        'not_found'             => __( 'Geen vacatures gevonden' ),
        'not_found_in_trash'    => __( 'Geen vacatures gevonden in prullenbak' )
    );

    $vacancy_args = array(
        'description'           => __( 'Vacatures' ),
        'labels'                => $vacancy_labels,
        'public'                => true,
        'show_ui'               => true,
        'menu_position'         => 29,
        'supports'              => array('title', 'excerpt', 'revisions', 'author'),
        'menu_icon'             => 'dashicons-search',
        'taxonomies'            => array('employment', 'location', 'discipline'),
        'rewrite'               => array( 'slug' => 'vacatures', 'with_front' => false ),
    );

    register_post_type( 'vacancy', $vacancy_args );

    /**
     * Register state taxonomy
     */
    register_taxonomy( 'state', array('vacancy'), array(
        'hierarchical' => false,
        'labels' => array(
            'name' => __( 'Status', 'carerixconnection' ),
            'singular_name' => __( 'status', 'carerixconnection' ),
            'all_items' => __( 'Alle statussen', 'carerixconnection' ),
        ),
        'public' => true,
        'show_ui' => false

    ) );

    // Seed states taxonomy with terms
    wp_insert_term(__( 'Actueel', 'carerixconnection' ), 'state' );
    wp_insert_term(__( 'Verwacht', 'carerixconnection' ), 'state', ['description' => __( '(verwacht)', 'carerixconnection' )] );
    wp_insert_term(__( 'Reactietermijn gesloten', 'carerixconnection' ), 'state', ['description' => __( '(gesloten)', 'carerixconnection' )] );
    wp_insert_term(__( 'Ingevuld', 'carerixconnection' ), 'state', ['description' => __( '(ingevuld)', 'carerixconnection' )] );

    /**
     * Register employment taxonomy
     */
    register_taxonomy( 'employment', array('vacancy'), array(
        'hierarchical' => false,
        'labels' => array(
            'name' => __( 'Dienstverband', 'carerixconnection' ),
            'singular_name' => __( 'dienstverband', 'carerixconnection' ),
            'all_items' => __( 'Alle dienstverbanden', 'carerixconnection' ),
        ),
        'public' => true,
        'show_ui' => false

    ) );

    /**
     * Register branche taxonomy
     */
    register_taxonomy( 'branche', array('vacancy'), array(
        'hierarchical' => false,
        'labels' => array(
            'name' => __( 'Sector', 'carerixconnection' ),
            'singular_name' => __( 'sector', 'carerixconnection' ),
            'all_items' => __( 'Alle sectoren', 'carerixconnection' ),
        ),
        'public' => true,
        'show_ui' => false

    ) );

    /**
     * Register discipline taxonomy
     */
    register_taxonomy( 'discipline', array('vacancy'), array(
        'hierarchical' => false,
        'labels' => array(
            'name' => __( 'Vakgebied', 'carerixconnection' ),
            'singular_name' => __( 'vakgebied', 'carerixconnection' ),
            'all_items' => __( 'Alle vakgebieden\'s', 'carerixconnection' ),
        ),
        'public' => true,
        'show_ui' => false

    ) );
    
    /**
     * Register location taxonomy
     */
    register_taxonomy( 'location', array('vacancy'), array(
        'hierarchical' => false,
        'labels' => array(
            'name' => __( 'Regio', 'carerixconnection' ),
            'singular_name' => __( 'regio', 'carerixconnection' ),
            'all_items' => __( 'Alle regio\'s', 'carerixconnection' ),
        ),
        'public' => true,
        'show_ui' => false

    ) );
}

/**
 * Set and unset admin columns for vacancy post type
 * @param array $columns
 * @return array
 */
function set_vacancy_admin_columns($columns) {

    unset( $columns['author'] );
    $rebuild_columns = array();
    foreach($columns as $key => $title) {
        if ($key=='date') {
            $rebuild_columns['carerix'] = __( 'Carerix', 'carerixconnection' );
            $rebuild_columns['pub_id'] = __( 'Publicatie ID', 'carerixconnection' );
        }
        
        $rebuild_columns[$key] = $title;
    }
    
    return $rebuild_columns;
}

/**
 * Populate custom admin columns with data
 * @param  string $column  
 * @param  int $post_id 
 * @return void          
 */
function fill_vacancy_admin_columns($column, $post_id) {

    $pub_id = get_post_meta($post_id, 'publication_id', true);
    if($pub_id) {

        switch ( $column ) {

            case 'carerix' :

                echo '&#10003;';
                break;

            case 'pub_id' :

                echo $pub_id;
                
                break;
        }
    }
}