<?php

/*
Plugin Name: Carerix Connection
Plugin URI:  https://upside.nl/
Description: Fetch vacancies from Carerix and send new applications
Version:     2.2.0
Author:      Upside
*/

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

if ( ! class_exists( 'CarerixConnection' ) ) {

    class CarerixConnection {

        public function __construct() {

            $this->setup();

            // Setup cron schedule to run every 4 times daily
            // add_filter('cron_schedules', array($this, 'cron_add_fourtimesdaily'));
            add_filter('cron_schedules', array($this, 'cron_add_everytenminutes'));

        }

        /**
         * Include files and setup hooks
         */
        public function setup() {
            
            // Setup admin settings
            if(is_admin()) {

                // Create settings
                new CarerixConnection\Admin\Settings();

                // Create Management page
                new CarerixConnection\Admin\Management();                

            }

            // Setup vacancy CPT
            include_once( plugin_dir_path( __FILE__ ) . 'includes/Vacancies/cpt.php');

            // Setup vacancy ACF Fields
            include_once( plugin_dir_path( __FILE__ ) . 'includes/Vacancies/fields.php');

        }

        /**
         * Add four times daily cron schedule
         * @param  array $schedules
         * @return array
         */
        public function cron_add_fourtimesdaily($schedules) {
           $schedules['fourtimesdaily'] = array(
               'interval' => 6 * 60 * 60,
               'display' => __( 'Viermaal per dag' )
           );
           return $schedules;
        }

        /**
         * Add every 10 minuters cron schedule
         * @param  array $schedules
         * @return array
         */
        public function cron_add_everytenminutes($schedules) {
           $schedules['everytenminutes'] = array(
               'interval' => 10 * 60,
               'display' => __( 'Elke tien minuten' )
           );
           return $schedules;
        }
    };

    // Instantiate the plugin class
    $new_carerix_connection = new CarerixConnection();
}

// Setup sync cron job
if ( ! wp_next_scheduled('run_vacancies_sync')) {

  wp_schedule_event(strtotime('03:00:00'), 'everytenminutes', 'run_vacancies_sync');

}

// Callback for run_vacancies event
add_action('run_vacancies_sync', function() {

    $synchronize = new CarerixConnection\Admin\Synchronize();

    $synchronize->sync_vacancies();

});